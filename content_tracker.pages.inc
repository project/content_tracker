<?php

/**
 * @file
 * User page callbacks for the content_tracker module.
 */

/**
 * Prints a listing of active nodes on the site.
 */
function content_tracker_page($account = NULL) {

  drupal_add_css(drupal_get_path('module', 'content_tracker') . '/content_tracker.css', 'module', 'all', FALSE);
  $nodes_per_page = variable_get('content_tracker_pager', 25);

  $sql = "SELECT t2n.logid, t2n.changed AS last_activity FROM {content_tracker_log} t2n ORDER BY t2n.changed DESC";
  $sql = db_rewrite_sql($sql, 't2n');
  $sql_count = "SELECT COUNT(n.logid) FROM {content_tracker_log} n";
  $sql_count = db_rewrite_sql($sql_count);
  $result = pager_query($sql, $nodes_per_page, 0, $sql_count);

  // This array acts as a placeholder for the data selected later
  // while keeping the correct order.
  $nodes = array();
  while ($node = db_fetch_object($result)) {
    $nodes[$node->logid] = $node;
  }

  return theme('content_tracker_page', $nodes);
}

/**
 * Override-able output of the content_tracker listings.
 */
function theme_content_tracker_page($nodes = array()) {

  if (!empty($nodes)) {

    // Now, get the data and put into the placeholder array.
    $placeholders = array_fill(0, count($nodes), '%d');
    $result = db_query(
            "SELECT n.logid, n.msg, n.ipaddress, n.actiondate, n.changed FROM {content_tracker_log} n
    WHERE n.logid IN(" . implode(',', $placeholders) . ")", array_keys($nodes)
      );
    // Finally display the data.
    $rows = array();
    while ($node = db_fetch_object($result)) {

      $nodes[$node->logid] = $node;
    }

    // Finally display the data.
    $rows = array();
    foreach ($nodes as $node) {
      $rows[] = array(
        check_plain($node->msg), check_plain($node->ipaddress), check_plain($node->actiondate),
      );
    }
  }
  else {
    $rows[] = array(
      array(
        'data' => t('No Logs available.'),
        'colspan' => '5',
      ),
    );
  }

  $header = array(
    t('Log Message'), t('IP Address'), t('Action Date'),
  );

  $output = '<div id="tracker">';
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, variable_get('content_tracker_pager', 25), 0);
  $output .= '</div>';
  return $output;
}
