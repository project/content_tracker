<?php

/**
 * @file
 * Admin page callbacks for the content_tracker module.
 */

/**
 * Creates the content_tracker administration form.
 */
function content_tracker_admin() {

  $max_nid = variable_get('content_tracker_index_nid', 0);
  $form['max_nid'] = array(
    '#value' => $max_nid,
    '#type'  => 'hidden',
  );

  $form['content_tracker_batch_size'] = array(
    '#title' => t('Number of log messages to store'),
    '#description' => t('Checks during each cron run.'),
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 7,
    '#default_value' => variable_get('content_tracker_batch_size', 1000),
    '#required' => TRUE,
  );

  $form['content_tracker_pager'] = array(
    '#title' => t('Nodes per page'),
    '#description' => t('Number of nodes to show per page of the tracker listing.'),
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 7,
    '#default_value' => variable_get('content_tracker_pager', 25),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validate callback.
 */
function content_tracker_admin_validate($form, &$form_state) {

  // Max_nid is just a markup field and should not cause a variable to be set.
  unset($form_state['values']['max_nid']);

  // The variables must be non-negative and numeric.
  if (!is_numeric($form_state['values']['content_tracker_batch_size']) || $form_state['values']['content_tracker_batch_size'] <= 0) {
    form_set_error('content_tracker_batch_size', t('The batch size must be a number and greater than zero.'));
  }
  if (!is_numeric($form_state['values']['content_tracker_pager']) || $form_state['values']['content_tracker_pager'] <= 0) {
    form_set_error('content_tracker_pager', t('The nodes per page must be a number and greater than zero.'));
  }
}
