                                                                                
General Information
-------------------
This module tracks all the nodes. It displays the user information who 
created,updated and deleted a node. The user information includes the 
IP address from which the modification done and the node information 
includes the node ID.

This module also tracks all the modules. It displays the user information 
who enabled and disabled a module. The user information includes the 
IP address from which the modification done and the module information 
includes the module name.

It also provides the pagination with admin configurable options. It also 
provides an option to specify number of log messages to store that will be 
checked during each cron run.It tracks the users who deleted content type 
as well.

https://www.drupal.org/project/content_tracker

How to use
----------

{site-url}/admin/log_messages to view all the log messages.
{site-url}/admin/settings/content_tracker to configure the settings.

Drupal Module Author
--------------------

Shyam Kumar Kunkala (shyam kunkala)
https://drupal.org/user/2408974
